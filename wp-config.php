<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cicd' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'b}@iDMX9XcaOz28CnAn7?2^[Dj1#{lS$SsvC~68ic$GPI4$!h)j63lnCmpXn^/-|' );
define( 'SECURE_AUTH_KEY',  '~d:psCu1Pv.mu>,47&w~,jCE0iu=E^j7ZYR>+c}78%4%]{C)*>c9c#6b&=KNcg*j' );
define( 'LOGGED_IN_KEY',    'BR,=eloaR B&1x,*?D;GU1kWQ=>$AXd+WtrT3a%uet[|bl)s%,NL[F]~4Kk|6S7o' );
define( 'NONCE_KEY',        'nRU*8Cm7zr=Nd`B].fzd#sNE9vw.5zY2KGeY]2wLb|a$_Q5p5DEo^-!?UbH5@E_d' );
define( 'AUTH_SALT',        'j;@klnpC>3zamy4oPAi-m~]c[4D2VncM4 ])&3jrZ:S7sfeV*. xRtM8^3vKST*o' );
define( 'SECURE_AUTH_SALT', 'UN@WLf5p`2@MHChG{GfDfX(~HKO,Og[1.]-11yUp$eNrztm=RSjLeD w}BnP,@*r' );
define( 'LOGGED_IN_SALT',   'SdG n1XOqPV)mfKWl+xfFJ`Z`PZJ{;9nk<B<JeD!3^4%U0bA} 0tp&m_nMor?dkD' );
define( 'NONCE_SALT',       '&`yF[e|xU4&drdycrDQ00K{oWZt@[t0&B?I(fstbjcc!_nP t)}qVKj*u}]/zVOS' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
